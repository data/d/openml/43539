# OpenML dataset: SBA-Loans-Case-Data-Set

https://www.openml.org/d/43539

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Should This Loan be Approved or Denied?
If you like the data set and download it, an upvote would be appreciated.
The Small Business Administration (SBA) was founded in 1953 to assist small businesses in obtaining loans. Small businesses have been the primary source of employment in the United States.  Helping small businesses help with job creation, which reduces unemployment.  Small business growth also promotes economic growth.  One of the ways the SBA helps small businesses is by guaranteeing bank loans. This guarantee reduces the risk to banks and encourages them to lend to small businesses.  If the loan defaults, the SBA covers the amount guaranteed, and the bank suffers a loss for the remaining balance.
There have been several small business success stories like FedEx and Apple.  However, the rate of default is very high.  Many economists believe the banking market works better without the assistance of the SBA.  Supporter claim that the social benefits and job creation outweigh any financial costs to the government in defaulted loans.
The Data Set
The original data set is from the U.S.SBA loan database, which includes historical data from 1987 through 2014 (899,164 observations) with 27 variables. The data set includes information on whether the loan was paid off in full or if the SMA had to charge off any amount and how much that amount was.  The data set used is a subset of the original set. It contains loans about the Real Estate and Rental and Leasing industry in California. This file has 2,102 observations and 35 variables. The column Default is an integer of 1 or zero, and I had to change this column to a factor.
For more information on this data set go to https://amstat.tandfonline.com/doi/full/10.1080/10691898.2018.1434342

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43539) of an [OpenML dataset](https://www.openml.org/d/43539). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43539/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43539/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43539/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

